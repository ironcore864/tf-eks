provider "aws" {
  version = "~> 2.55"
  region  = "us-west-2"
}

# Additional providers configuration for eu-central-1 and eu-west-2, used for dynamodb global table
provider "aws" {
  alias  = "usw1"
  region = "us-west-1"
}

provider "aws" {
  alias  = "usw2"
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "s3-sei-tf-state-us-west-2"
    key            = "tfstate"
    region         = "us-west-2"
    dynamodb_table = "ddb_sei_terraform_lock"
  }
}
