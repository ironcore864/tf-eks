cluster_name = "sei-test"

public_subnet_zone_cidr_map = {
  "us-west-2a" : "10.0.1.0/24",
  "us-west-2b" : "10.0.2.0/24",
  "us-west-2c" : "10.0.3.0/24"
}

eks_subnet_zone_cidr_map = {
  "us-west-2a" : "10.0.5.0/24",
  "us-west-2b" : "10.0.6.0/24",
  "us-west-2c" : "10.0.7.0/24"
}

repos = [
  "skyway-iot-gateway",
  "skyway-iot-device",
  "skyway-iot-mqtt-cluster-server"
]
