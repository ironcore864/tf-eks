variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnet_zone_cidr_map" {
  type = map(string)
}

variable "eks_subnet_zone_cidr_map" {
  type = map(string)
}

variable "cluster_name" {
  type = string
}
