resource "aws_subnet" "public" {
  for_each = var.public_subnet_zone_cidr_map

  vpc_id            = aws_vpc.main.id
  cidr_block        = each.value
  availability_zone = each.key

  # lifecycle {
  #   ignore_changes = [tags]
  # }

  tags = {
    Name                                                 = "public"
    format("kubernetes.io/cluster/%s", var.cluster_name) = "shared"
    "kubernetes.io/role/elb"                             = "1"
  }
}

resource "aws_subnet" "eks" {
  for_each = var.eks_subnet_zone_cidr_map

  vpc_id            = aws_vpc.main.id
  cidr_block        = each.value
  availability_zone = each.key

  lifecycle {
    ignore_changes = [tags]
  }

  tags = {
    Name = "eks"
  }
}
