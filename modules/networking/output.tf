output "eks_subnet_ids" {
  value = [
    for key, value in aws_subnet.eks :
    value.id
  ]
}
