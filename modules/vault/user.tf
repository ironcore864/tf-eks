data "aws_iam_policy_document" "vault-kms-unseal" {
  statement {
    sid       = "VaultKMSUnseal"
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:DescribeKey",
    ]
  }
}

resource "aws_iam_policy" "vault-kms-unseal" {
  name   = "vault-kms-unseal"
  policy = data.aws_iam_policy_document.vault-kms-unseal.json
}

data "aws_iam_policy_document" "vault-dynamodb-backend" {
  statement {
    sid       = "VaultDynamoDBl"
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "dynamodb:*"
    ]
  }

  statement {
    effect    = "Deny"
    resources = ["*"]

    actions = [
      "dynamodb:DeleteTable",
      "dynamodb:DeleteBackup"
    ]
  }
}

resource "aws_iam_policy" "vault-dynamodb-backend" {
  name   = "vault-dynamodb-backend"
  policy = data.aws_iam_policy_document.vault-dynamodb-backend.json
}

resource "aws_iam_user" "vault-dynamodb-kms" {
  name = "vault-dynamodb-kms"
}

resource "aws_iam_access_key" "vault-dynamodb-kms" {
  user = aws_iam_user.vault-dynamodb-kms.name
}

resource "aws_iam_user_policy_attachment" "dynamodb" {
  user       = aws_iam_user.vault-dynamodb-kms.name
  policy_arn = aws_iam_policy.vault-dynamodb-backend.arn
}

resource "aws_iam_user_policy_attachment" "kms" {
  user       = aws_iam_user.vault-dynamodb-kms.name
  policy_arn = aws_iam_policy.vault-kms-unseal.arn
}
