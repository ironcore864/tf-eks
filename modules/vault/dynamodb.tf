module "dynamodb-global" {
  source = "../dynamodb-global"

  table_name   = var.table_name
  region1_name = var.region1_name
  region2_name = var.region2_name

  providers = {
    aws.region1 = aws.region1
    aws.region2 = aws.region2
  }
}
