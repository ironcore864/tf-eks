output "aws_access_key_id" {
  value = aws_iam_access_key.vault-dynamodb-kms.id
}

output "aws_secret_access_key" {
  value = aws_iam_access_key.vault-dynamodb-kms.secret
}

output "kms_key_id" {
  value = aws_kms_key.vault.key_id
}

output "table_id" {
  value = module.dynamodb-global.table_id
}

output "vault_aws_engine_user_access_key_id" {
  value = aws_iam_access_key.vault_iam_engine.id
}

output "vault_aws_engine_user_secret_access_key" {
  value = aws_iam_access_key.vault_iam_engine.secret
}
