resource "aws_kms_key" "vault" {
  description             = "Vault unseal key"
  deletion_window_in_days = 10

  tags = {
    Name = "vault-kms-unseal"
  }
}

resource "aws_kms_grant" "vault" {
  name              = "grant-vault-access"
  key_id            = aws_kms_key.vault.key_id
  grantee_principal = aws_iam_user.vault-dynamodb-kms.arn
  operations        = ["Encrypt", "Decrypt", "DescribeKey", "GenerateDataKey"]
}
