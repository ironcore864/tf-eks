data "aws_caller_identity" "current" {}

resource "aws_iam_user" "vault_iam_engine" {
  name = "vault-iam-engine-user"
  path = "/"
}

resource "aws_iam_access_key" "vault_iam_engine" {
  user = aws_iam_user.vault_iam_engine.name
}

resource "aws_iam_user_policy" "vault_iam_engine" {
  name = "test"
  user = aws_iam_user.vault_iam_engine.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "iam:AttachUserPolicy",
        "iam:CreateAccessKey",
        "iam:CreateUser",
        "iam:DeleteAccessKey",
        "iam:DeleteUser",
        "iam:DeleteUserPolicy",
        "iam:DetachUserPolicy",
        "iam:ListAccessKeys",
        "iam:ListAttachedUserPolicies",
        "iam:ListGroupsForUser",
        "iam:ListUserPolicies",
        "iam:PutUserPolicy",
        "iam:RemoveUserFromGroup"
      ],
      "Resource": ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/vault-*"]
    }
  ]
}
EOF
}
