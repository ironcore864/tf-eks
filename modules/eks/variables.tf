variable "cluster_name" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "eks_cluster_version" {
  type    = string
  default = "1.16"
}
