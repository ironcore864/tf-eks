output "endpoint" {
  value = aws_eks_cluster.eks.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.eks.certificate_authority.0.data
}

output "cluster_identity_oidc_issuer" {
  value = aws_eks_cluster.eks.identity[0].oidc[0].issuer
}
