resource "aws_eks_cluster" "eks" {
  depends_on = [aws_iam_role_policy_attachment.AmazonEKSClusterPolicy, aws_iam_role_policy_attachment.AmazonEKSServicePolicy]

  name     = var.cluster_name
  role_arn = aws_iam_role.eks-cluster-role.arn
  version  = var.eks_cluster_version

  vpc_config {
    subnet_ids = var.subnet_ids
  }
}
