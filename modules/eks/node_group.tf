resource "aws_eks_node_group" "default" {
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly
  ]

  cluster_name    = aws_eks_cluster.eks.name
  node_group_name = "eks-${var.cluster_name}-node-group"
  node_role_arn   = aws_iam_role.eks-node-role.arn
  subnet_ids      = var.subnet_ids

  scaling_config {
    desired_size = 2
    max_size     = 2
    min_size     = 2
  }
}
