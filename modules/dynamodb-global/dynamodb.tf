resource "aws_dynamodb_table" "region1" {
  provider = aws.region1

  hash_key  = "Path"
  range_key = "Key"

  point_in_time_recovery {
    enabled = true
  }

  name             = var.table_name
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  read_capacity    = 5
  write_capacity   = 5

  attribute {
    name = "Path"
    type = "S"
  }

  attribute {
    name = "Key"
    type = "S"
  }
}

resource "aws_dynamodb_table" "region2" {
  provider = aws.region2

  hash_key         = "Path"
  range_key        = "Key"
  name             = var.table_name
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  read_capacity    = 5
  write_capacity   = 5

  attribute {
    name = "Path"
    type = "S"
  }

  attribute {
    name = "Key"
    type = "S"
  }
}

resource "aws_dynamodb_global_table" "global" {
  provider = aws.region2

  depends_on = [
    aws_dynamodb_table.region1,
    aws_dynamodb_table.region2,
  ]

  name = var.table_name

  replica {
    region_name = var.region1_name
  }

  replica {
    region_name = var.region2_name
  }
}
