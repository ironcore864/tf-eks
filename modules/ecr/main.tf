resource "aws_ecr_repository" "foo" {
  for_each = toset(var.repos)

  name = each.key

  image_scanning_configuration {
    scan_on_push = true
  }
}
