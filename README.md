# AWS EKS with Terraform

## Dependencies

terraform >= 0.12.23

## Manual Steps

Do this only for a completely new account:

- Create s3 bucket: s3-sei-tf-state-us-west-2 (must be a different name for each account, used in config.tf)
- Create DynamoDB: ddb_sei_terraform_lock with primary key named LockID (used in config.tf)

## Deploy

You need to have aws region/access key id/secret configured first, located in ~/.aws/config and ~/.aws/credentials.

Then run:

```bash
terraform init
terraform plan -out tfplan
terraform apply tfplan
```

## Access

```
aws eks update-kubeconfig --region=us-west-2 --name sei-test
```

## Todo

- external-dns
- ingress controller
