module "networking" {
  source = "./modules/networking"

  vpc_cidr_block              = var.vpc_cidr_block
  public_subnet_zone_cidr_map = var.public_subnet_zone_cidr_map
  eks_subnet_zone_cidr_map    = var.eks_subnet_zone_cidr_map
  cluster_name                = var.cluster_name
}

module "eks" {
  source = "./modules/eks"

  cluster_name = var.cluster_name
  subnet_ids   = module.networking.eks_subnet_ids
}

module "ecr" {
  source = "./modules/ecr"

  repos = var.repos
}

resource "aws_iam_openid_connect_provider" "eks" {
  client_id_list = ["sts.amazonaws.com"]
  # https://github.com/terraform-providers/terraform-provider-aws/issues/10104
  thumbprint_list = ["9e99a48a9960b14926bb7f3b02e22da2b0ab7280"]
  url             = module.eks.cluster_identity_oidc_issuer
}

module "s3-helm-chart-repo" {
  source = "./modules/s3-helm-chart-repo"
}

module "external-dns" {
  source = "./modules/external-dns"

  cluster_name  = var.cluster_name
  oidc_provider = module.eks.cluster_identity_oidc_issuer
}

module "alb" {
  source = "./modules/alb"

  cluster_name  = var.cluster_name
  oidc_provider = module.eks.cluster_identity_oidc_issuer
}

module "vault" {
  source = "./modules/vault"

  table_name   = var.vault_dynamo_table_name
  region1_name = var.dynamodb_region_1_name
  region2_name = var.dynamodb_region_2_name

  providers = {
    aws.region1 = aws.usw1
    aws.region2 = aws.usw2
  }
}
