variable "cluster_name" {
  type = string
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnet_zone_cidr_map" {
  type = map(string)
}

variable "eks_subnet_zone_cidr_map" {
  type = map(string)
}

variable "repos" {
  type = list(string)
}

variable "vault_dynamo_table_name" {
  type    = string
  default = "vault-data"
}

variable "dynamodb_region_1_name" {
  type    = string
  default = "us-west-1"
}

variable "dynamodb_region_2_name" {
  type    = string
  default = "us-west-2"
}
