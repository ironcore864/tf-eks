output "alb_iam_role_arn" {
  value = module.alb.alb_iam_role_arn
}

output "external_dns_iam_role_arn" {
  value = module.external-dns.external_dns_iam_role_arn
}

output "helm_chart_repo_s3_bucket_name" {
  value = module.s3-helm-chart-repo.bucket_name
}

# Technical IAM User for Vault to access Dynamodb/KMS
output "vault_iam_user_aws_access_key_id" {
  value = module.vault.aws_access_key_id
}

output "vault_iam_user_aws_secret_access_key" {
  value = module.vault.aws_secret_access_key
}

# Technical IAM User for Vault to manage AWS engine
output "vault_aws_engine_user_access_key_id" {
  value = module.vault.vault_aws_engine_user_access_key_id
}

output "vault_aws_engine_user_secret_access_key" {
  value = module.vault.vault_aws_engine_user_secret_access_key
}

# DynamoDB
output "vault_dynamodb_table_id" {
  value = module.vault.table_id
}

# KMS
output "vault_kms_key_id" {
  value = module.vault.kms_key_id
}
